<?php
/**
 * @file
 *   An remoteimport of the provision service API.
 *
 *  Declares a new service type and a basic implementation of it.
 *  It matches the same service definition in the hosting front end.
 */

/**
 * Implements hook_drush_init().
 */
function remoteimport_drush_init() {
  remoteimport_register_autoload();
}

/**
 * Register our directory as a place to find provision classes.
 */
function remoteimport_register_autoload() {
  static $loaded = FALSE;
  if (!$loaded) {
    $loaded = TRUE;
    provision_autoload_register_prefix('Provision_', dirname(__FILE__));
  }
}

/**
 * Implements hook_provision_services().
 */
function remoteimport_provision_services() {
  remoteimport_register_autoload();
  return array('remoteimport' => NULL);
}

/**
 * Implements hook_drush_command().
 */
function remoteimport_drush_command() {
  $items = array();
   
  $items['provision-remoteimport_list_sites'] = array(
    'description' => "List all sites on the remote server.",
    'arguments' => array(
    ),
    'options' => array(
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
  
  $items['provision-remoteimport'] = array(
    'description' => "Imports a site from the remote server.",
    'arguments' => array(
    ),
    'options' => array(
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
   
  return $items;
}

function drush_remoteimport_provision_remoteimport_list_sites() {
  $sites = d()->service('remoteimport')->list_sites();
  if (is_array($sites)) {
    drush_set_option('remote_sites_list', $sites);
  }
  else {
    return drush_set_error('No sites found');
  }
}

function drush_remoteimport_provision_remoteimport() {
  $old_url = drush_get_option('old_url');
  
  // Go do a backup of that site.
  $backup_file = d()->service('remoteimport')->fetch_site($old_url);
  
  // Now we want to deploy that backup with the given options.
  $new_alias = d()->service('remoteimport')->deploy($backup_file, drush_get_option('old_url'), drush_get_option('new_url'), drush_get_option('platform'), drush_get_option('db_server'));
  
  // And finally, we should import this back into Hostmaster.
  provision_backend_invoke('@hostmaster', 'hosting-import', array($new_alias), array('remoteimport_import' => TRUE));
}

